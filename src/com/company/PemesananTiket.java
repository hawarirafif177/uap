package com.company;

class PemesananTiket {
    // Do your magic here...
    private String namaPemesan;
    private String jenisTiket;
    private int jumlahTiket;
    private String kodePesanan;
    private String tanggalPesanan;
    private int hargaTiket;

    public PemesananTiket(String namaPemesan, String jenisTiket, int jumlahTiket) {
        this.namaPemesan = namaPemesan;
        this.jenisTiket = jenisTiket;
        this.jumlahTiket = jumlahTiket;
    }

    public void pesanTiket() {
        kodePesanan = Main.generateKodeBooking();
        tanggalPesanan = Main.getCurrentDate();
    }

    public void tampilkanDetailPesanan() {
        System.out.println("Detail Pesanan:");
        System.out.println("Nama Pemesan: " + namaPemesan);
        System.out.println("Kode Pesanan: " + kodePesanan);
        System.out.println("Tanggal Pesanan: " + tanggalPesanan);
        System.out.println("Jenis Tiket: " + jenisTiket);
        System.out.println("Jumlah Tiket: " + jumlahTiket);
        System.out.println("Harga Tiket: " + hargaTiket);
    }
}





