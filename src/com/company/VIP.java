package com.company;

class VIP extends TiketKonser {
    // Do your magic here...

    public VIP(String namaPemesan, int jumlahTiket, int hargaTiket) {
        super(namaPemesan, jumlahTiket, hargaTiket);
    }

    @Override
    public double getHarga() {
        return 3000000;
    }
}
