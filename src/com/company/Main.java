

/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */
package com.company;
import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        //Do your magic here...
            Scanner scanner = new Scanner(System.in);

            System.out.print("Masukkan nama pemesan: ");
            String namaPemesan = scanner.nextLine();

            System.out.println("Jenis tiket yang tersedia: ");
            System.out.println("1. CAT8");
            System.out.println("2. CAT1");
            System.out.println("3. Festival");
            System.out.println("4. VIP");
            System.out.println("5. VVIP");

            System.out.print("Masukkan pilihan tiket (1-5): ");
            int pilihan = scanner.nextInt();
            scanner.nextLine();

            String jenisTiket;

            switch (pilihan) {
                case 1:
                    jenisTiket = "CAT8";
                    break;
                case 2:
                    jenisTiket = "CAT1";
                    break;
                case 3:
                    jenisTiket = "FESTIVAL";
                    break;
                case 4:
                    jenisTiket = "VIP";
                    break;
                case 5:
                    jenisTiket = "VVIP";
                    break;
                default:
                    System.out.println("Pilihan tiket tidak valid. Pembelian tiket dibatalkan.");
                    scanner.close();
                    return;
            }

            System.out.print("Masukkan jumlah tiket: ");
            int jumlahTiket = scanner.nextInt();

            PemesananTiket pemesanan = new PemesananTiket(namaPemesan, jenisTiket, jumlahTiket);
            pemesanan.pesanTiket();
            pemesanan.tampilkanDetailPesanan();

            scanner.close();
        }


    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}
