package com.company;

abstract class TiketKonser implements HargaTiket {
    String namaPemesan;
    int jumlahTiket;
    int hargaTiket;

    public TiketKonser(String namaPemesan, int jumlahTiket, int hargaTiket) {
        this.namaPemesan = namaPemesan;
        this.jumlahTiket = jumlahTiket;
        this.hargaTiket = hargaTiket;
    }

    public abstract double getHarga();
}
