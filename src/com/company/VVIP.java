package com.company;

class VVIP extends TiketKonser {

    // Do your magic here...
    private final double harga = 800000;
    public VVIP(String namaPemesan, int jumlahTiket, int harga) {
        super(namaPemesan, jumlahTiket, harga);
    }

    @Override
    public double getHarga() {
        return 800000;
    }
}